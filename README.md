# UBlock Lists

This here are just my block lists. Some of them do not only block ads but also annoying please sign up messages.

## Installation

To install just copy the code below to the Custom section in the UBlock configuration
```
!List to block annoying things
https://gitlab.com/cromir/UBlock/raw/master/anti-annoy.filter.txt
```
